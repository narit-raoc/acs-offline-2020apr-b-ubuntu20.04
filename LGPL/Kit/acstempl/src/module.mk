SCRIPTS  := getTemplate			\
           getTemplateMainMenu 		\
           getTemplateForCoding		\
           getACSdirectoryStructure     \
           getTemplateForDirectory

#
# man pages to be done
# --------------------
MANSECTIONS = 5 7
MAN5 =  acsDirectoryStructure.doc
MAN7 =  getTemplate getTemplateForDirectory

LMODRULE=$(MODRULE)
LMODDEP=$(MODDEP)
LMODPATH=$(MODPATH)
$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
