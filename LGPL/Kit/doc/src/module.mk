EXECUTABLES:=docDoManPages docA2MIF docExDb docExPc \
             docDoDbTree docSplitIntoSingleComments docSetCommentNumber

#
# Man Pages extractor 
docDoManPages_OBJECTS := docDoManPages

#
# Filter to convert ASCII into MIF
docA2MIF_OBJECTS := docA2MIF

#
# Filter to extract db info
docExDb_OBJECTS := docExDb

#
# Filter to extract pseudo code
docExPc_OBJECTS := docExPc

#
# Display (DB) directory structure
docDoDbTree_OBJECTS := docDoDbTree

#
# doc review procedure tools
docSplitIntoSingleComments_OBJECTS := docSplitIntoSingleComments
docSetCommentNumber_OBJECTS := docSetCommentNumber

SCRIPTS  :=  docFMupdate docDos2Unix docExDbDir docDoDbInfo \
            docArchive doc docSelectDocument docSelectIssue \
            docSelectPreparation docSelectOption docMakeReport \
            docMergeComments docMoveOldVersions docCopyMif\
            docImported4to5.ex docImporting4to5.ex docModManBuild\
	    docDeroff docPrepareComments

#
# TCL scripts (public and local)
# ------------------------------
TCL_SCRIPTS     := #docPcode docManExtract docUnix2Dos
TCL_SCRIPTS_L   :=

#
# Gianluca's format Pseudo code extractor
docPcode_OBJECTS  := docPcode
docPcode_TCLSH    := tcl -f

docManExtract_OBJECTS := docManExtract
docManExtract_TCLSH   := seqSh

docUnix2Dos_OBJECTS   := docUnix2Dos
docUnix2Dos_TCLSH     := seqSh



#
#
# man pages to be done
# --------------------
MANSECTIONS:=7
MAN7:= docDoManPages.c docA2MIF.c docFMupdate docExDb.c docExDbDir \
       docExPc.c docDoDbTree.c docDoDbInfo docUnix2Dos.tcl docDos2Unix \
       doc docArchive docMergeComments docSetCommentNumber.c \
       docMoveOldVersions docCopyMif docManExtract docModManBuild docDeroff

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
