#XERCES_JARTMP := $(shell searchFile lib/endorsed/xercesImpl.jar)
#ifneq ($(XERCES_JARTMP),\#error\#)
#   XERCES_JAR := $(XERCES_JARTMP)/lib/endorsed/xercesImpl.jar
#endif

#CLASSPATH := $(CLASSPATH):$(XERCES_JAR)

IDL_FILES = HelloDemo XmlComponent LampAccess LampCallback ErrorSystemComponent EventComponent
IDL_TAO_FLAGS =
USER_IDL =

HelloDemoStubs_LIBS = acscomponentStubs

XmlComponentStubs_LIBS = acscomponentStubs JContExmplErrTypeTestStubs xmlentityStubs 

LampAccessStubs_LIBS = acscomponentStubs acserrStubs

LampCallbackStubs_LIBS = acscomponentStubs acserrStubs LampAccessStubs

ErrorSystemComponentStubs_LIBS = ErrorSystemExampleStubs acscomponentStubs

EventComponentStubs_LIBS = acsexmplFridgeStubs ACSErrTypeCommonStubs acscomponentStubs

ACSERRDEF = JContExmplErrTypeTest ErrorSystemExample

COMPONENTS_JARFILES = jcontexmplComp

jcontexmplComp_DIRS = \
	alma/demo/ErrorSystemComp \
	alma/demo/EventConsumerImpl \
	alma/demo/EventSupplierImpl \
	alma/demo/ComponentWithXmlOffshootImpl \
	alma/demo/HelloDemoImpl \
	alma/demo/LampAccessImpl \
	alma/demo/LampCallbackImpl \
	alma/demo/XmlComponentImpl \
	alma/demo/XmlOffShootImpl \
	alma/acsexmplErrorComponent
jcontexmplComp_JLIBS = LampAccess EventComponent ErrorSystemComponent LampCallback XmlComponent


JARFILES = jcontexmplClient

jcontexmplClient_DIRS = \
	alma/demo/client \
	alma/demo/dyncomp \
	alma/demo/EventConsumerImpl \
	alma/acsexmpl/clients
jcontexmplClient_JLIBS= XmlComponent LampCallback EventComponent

DEBUG = on
#COMPONENT_HELPERS=on

XML_IDL="ObsProposal=alma.xmljbind.test.obsproposal.ObsProposal; \
         SchedBlock=alma.xmljbind.test.schedblock.SchedBlock"

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
