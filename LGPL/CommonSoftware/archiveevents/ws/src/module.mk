INCLUDES        = archiveeventsArchiveSupplier.h archiveeventsExport.h

LIBRARIES       = archiveevents
LIBRARIES_L     =  

archiveevents_OBJECTS = archiveeventsArchiveSupplier
archiveevents_LIBS = acsncErrType cdb acsncStubs basenc
archiveevents_CFLAGS = -DARCHIVEEVENTS_BUILD_DLL

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
