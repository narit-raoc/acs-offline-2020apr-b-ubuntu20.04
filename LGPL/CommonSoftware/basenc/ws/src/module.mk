INCLUDES        = basencHelper.h basencSupplier.h

LIBRARIES       = basenc
LIBRARIES_L     =

basenc_OBJECTS = basencHelper basencSupplier
basenc_LIBS    = acsncStubs acscommonStubs baciStubs acscomponentStubs acserrStubs acserr \
					  acsutil cdb ACSErrTypeCORBA RepeatGuard

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
