CDB_SCHEMAS = CDB

ACSERRDEF = cdbErrType

IDL_FILES = cdbDAL cdbjDAL
IDL_TAO_FLAGS =
USER_IDL =
cdbDAL_IDLS:=acscommon logging_idl cdbErrType
cdbjDAL_IDLS:=cdbDAL

cdbDALStubs_LIBS = cdbErrTypeStubs acscomponentStubs
cdbjDALStubs_LIBS = cdbDALStubs

DEBUG=on

XSDBIND = cdbSchemaBindings

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
