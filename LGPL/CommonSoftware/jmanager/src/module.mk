ACSERRDEF = jmanagerErrType

SCRIPTS         = maciManagerJ
SCRIPTS_L       =

JARFILES = jManager
USER_JFLAGS=

jManager_DIRS = com
jManager_EXTRAS =
jManager_JLIBS=jmanagerErrType

DEBUG=on

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
