DEBUG=on
USER_INC = -I$(PYTHONINC)

INCLUDES        = 	acstimeClockImpl.h \
			acstimeTimerImpl.h \
			acstimeDevIOTime.h acstimeTimeUtil.h \
			acstimeDurationHelper.h acstimeEpochHelper.h \
			acstime.h \
			acstimeProfiler.h

LIBRARIES       = acstime acsclock acstimer _acstimeSWIG 
LIBRARIES_L     =

acstime_OBJECTS   =	acstimeTimeUtil \
			acstimeDurationHelper acstimeEpochHelper \
			acstimeProfiler \
			acstimeC acstimeS
acstime_LIBS      =     baci ACSTimeError maciErrType

acsclock_OBJECTS   =	acstimeClockImpl
acsclock_LIBS      =    baci ACSTimeError acstime


acstimer_OBJECTS   =	acstimeTimerImpl
acstimer_LIBS      =    baci ACSTimeError acstime

_acstimeSWIG_OBJECTS   = $(shell if [ $(shell uname) = Linux -o $(shell uname) = $(CYGWIN_VER) ]; then echo 'acstimeSWIG_wrap '; fi;) \
			 acstimeTimeUtil \
			 acstimeDurationHelper acstimeEpochHelper \
			 acstimeC acstimeS
_acstimeSWIG_LIBS      = baci ACSTimeError maciErrType python2.7

CDB_SCHEMAS = CLOCK

IDL_FILES = acstime
IDL_TAO_FLAGS =
USER_IDL =

acstimeStubs_LIBS = baciStubs ACSTimeErrorStubs

ACSERRDEF = ACSTimeError

JARFILES = jacstime
jacstime_JLIBS=acstime
jacstime_DIRS = alma

PY_MODULES         = acstimeSWIG
PY_MODULES_L       =

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)$(RM) $(INSTALL_ROOT)/lib/python/site-packages/_acstimeSWIG.$(SHLIB_EXT)
	$(AT)cp $(MODPATH)/lib/lib_acstimeSWIG.$(SHLIB_EXT) $(INSTALL_ROOT)/lib/python/site-packages/_acstimeSWIG.$(SHLIB_EXT)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"

acstime_swig:
	$(AT)swig -c++ -python $(MODPATH)/config/acstimeSWIG.i
	$(AT)mv $(MODPATH)/config/acstimeSWIG_wrap.cxx $(MODPATH)/src/acstimeSWIG_wrap.cpp
	$(AT)mv $(MODPATH)/config/acstimeSWIG.py $(MODPATH)/src/
