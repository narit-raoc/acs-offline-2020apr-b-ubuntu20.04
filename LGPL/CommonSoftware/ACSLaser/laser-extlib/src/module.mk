INSTALL_FILES =  ../lib/boot.jar \
                 ../lib/core.jar \
                 ../lib/core-windows.jar \
                 ../lib/jms.jar \
                 ../lib/openide.jar \
                 ../lib/openide-io.jar \
                 ../lib/mail.jar \
                 ../lib/selector-1.1.jar

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
