IDL_FILES = AlarmSystem
IDL_TAO_FLAGS =
USER_IDL =

AlarmSystemStubs_LIBS = AcsAlarmSystemStubs

JARFILES= lasercore

lasercore_DIRS=alma cern com
lasercore_JLIBS = AlarmSystem

DEBUG=on

XSDBIND=AlarmSysStatsSchemaBindings

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
