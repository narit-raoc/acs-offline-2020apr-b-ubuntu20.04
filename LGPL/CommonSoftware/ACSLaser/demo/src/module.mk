LIBRARIES       = alsysMountImpl
LIBRARIES_L     = 

alsysMountImpl_OBJECTS  = alsysMountImpl
alsysMountImpl_LIBS		= demoComponentsStubs acsAlSysSource acsErrTypeAlarmSourceFactory acscomponent archiveevents

PY_PACKAGES        = AlSysDemo
PY_PACKAGES_L      =

IDL_FILES = demoComponents
IDL_TAO_FLAGS =
USER_IDL =
demoComponentsStubs_LIBS = acscomponentStubs

JARFILES= asdemo
asdemo_DIRS=alma
asdemo_JLIBS = demoComponents

DEBUG=on

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
