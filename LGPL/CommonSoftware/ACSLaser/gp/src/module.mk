JARFILES = gp
gp_DIRS = cern
gp_EXTRAS:=$(shell find $(MODPATH)/src/cern -type f | grep -v CVS | grep -v ".java" |sed "s'$(MODPATH)/src/''") \
           $(shell find $(MODPATH)/src/org -type f | grep -v CVS | grep -v ".java" |sed "s'$(MODPATH)/src/''")

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
