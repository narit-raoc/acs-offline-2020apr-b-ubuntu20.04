JARFILES= alarmsysteminterface

alarmsysteminterface_DIRS= cern alma
alarmsysteminterface_EXTRAS= cern/laser/source/alarmsysteminterface/impl/asi-configuration.xml
alarmsysteminterface_JLIBS = AlarmSourceSchemaBindings

DEBUG= on

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
