#USER_CFLAGS = -fpic -rdynamic 

INCLUDES        = 	CERNAlarmSystemInterfaceProxy.h \
					AcsAlarmPublisher.h \
					AlarmSupplier.h \
					CERNASIMessage.h \
					utilConstants.h

LIBRARIES       = laserSourceAcsSpecific
LIBRARIES_L     =

laserSourceAcsSpecific_OBJECTS   = 	CERNASIMessage \
									CERNAlarmSystemInterfaceFactory \
									CERNAlarmSystemInterfaceProxy  \
									AcsAlarmPublisher \
									AlarmSupplier
laserSourceAcsSpecific_LIBS = basenc ACSJMSMessageEntityStubs acsnc acsAlSysSource errTypeAlarmService acsutil
#laserSourceAcsSpecific_LDFLAGS = -Wl,--export-dynamic -dl -fpic

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
