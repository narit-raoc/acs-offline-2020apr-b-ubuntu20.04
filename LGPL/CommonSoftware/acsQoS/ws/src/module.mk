INCLUDES        = acsQoS.h acsQoStimeout.h acsQoSExport.h

LIBRARIES       = acsQoS
LIBRARIES_L     =

acsQoS_OBJECTS   = acsQoS acsQoStimeout
acsQoS_LIBS = acsQoSErrType TAO_Messaging
acsQoS_CFLAGS = -DACSQOS_BUILD_DLL

ACSERRDEF = acsQoSErrType

DEBUG=on

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
