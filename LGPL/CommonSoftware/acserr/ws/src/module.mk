PY_PACKAGES = ACSErrorChecker
DEBUG=on

INCLUDES        = acserr.h acserrExceptionManager.h acserrLegacy.h acserrACSbaseExImpl.h \
		 acserrHandlers.h acserrGenExport.h

LIBRARIES       = acserr
LIBRARIES_L     =

acserr_OBJECTS = acserr acserrExceptionManager \
		 acserrLegacy acserrACSbaseExImpl \
		 acserrHandlersErr acserrHandlers ErrorSystemErrType
acserr_LIBS = logging loki acsutil acserrStubs acserrHandlersErrStubs
acserr_CFLAGS = -DACSERRGEN_BUILD_DLL

SCRIPTS         = acserrGenCpp acserrGenJava acserrGenIDL acserrGenPython updateErrDefs.sh acserrGenCheckXML
SCRIPTS_L       =

acserrGenCheckXML_DEPS:=$(mod_acserr_name)_xmlvalidator_jar

INSTALL_FILES = ../config/AES2H.xslt ../config/AES2CPP.xslt ../config/AES2IDL.xslt ../config/AES2Java.xslt ../config/AES2Py.xslt ../idl/ACSError.xsd

IDL_FILES =

XSDBIND = ACSError
XSDBIND_INCLUDE = commontypes

JARFILES = acserrj xmlvalidator
acserrj_DIRS = alma/acs/exceptions
acserrj_JARS:=jACSUtil
xmlvalidator_DIRS = alma/acs/xml/validator

ACSERRDEF:=acserrHandlersErr ErrorSystemErrType

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
