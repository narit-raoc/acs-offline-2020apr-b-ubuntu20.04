CDB_SCHEMAS = Container Components Component HierarchicalComponent Manager LoggingConfig

IDL_FILES = maci
IDL_TAO_FLAGS =
USER_IDL =

maciStubs_LIBS = logging_idlStubs ACSErrTypeCommon maciErrTypeStubs acscommonStubs

ACSERRDEF = maciErrType

DEBUG=on 

XSDBIND = maciSchemaBindings
XSDBIND_INCLUDE=cdbSchemaBindings

INSTALL_FILES = ../lib/maciSchemaBindings.jar

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
