EXECUTABLES     = taskRunner
EXECUTABLES_L   = 

taskRunner_OBJECTS   = taskRunner
taskRunner_LIBS      = taskComponentStubs staticContainer characteristicTaskStubs taskErrType baciStubs xerces-c

INCLUDES        = taskComponent.h characteristicTask.h parameterTask.h

LIBRARIES       = staticContainer taskComponent characteristicTask parameterTask
LIBRARIES_L     =

taskComponent_OBJECTS   = taskComponent
taskComponent_LIBS      = staticContainer taskComponentStubs taskErrType maci xerces-c

parameterTask_OBJECTS= parameterTask
parameterTask_LIBS= taskComponent parameter acsXercesUtilities maci xerces-c

characteristicTask_LIBS =  taskComponent characteristicTaskStubs baciStubs baci maci xerces-c

staticContainer_OBJECTS = taskStaticContainer taskStaticContainerServices
staticContainer_LIBS = maci xerces-c

IDL_FILES = taskComponent characteristicTask
IDL_TAO_FLAGS =
USER_IDL =

taskComponent_IDLS:=acscomponent taskErrType
characteristicTask_IDLS:=baci taskComponent

taskComponentStubs_LIBS = acscomponentStubs taskErrType
characteristicTaskStubs_LIBS = taskComponentStubs baciStubs

ACSERRDEF = taskErrType

DEBUG=on 

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
