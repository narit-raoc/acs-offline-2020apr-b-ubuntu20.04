#USER_INC = -I$(TAO_ROOT)/orbsvcs/orbsvcs/AV
#USER_LIB =

INCLUDES        = bulkDataReceiverImpl.h bulkDataReceiverImpl.i \
		  bulkDataReceiver.h bulkDataReceiver.i \
	          bulkDataSender.h bulkDataSender.i \
		  bulkDataSenderImpl.h bulkDataSenderImpl.i \
		  bulkDataSenderDefaultCb.h bulkDataCallback.h \
		  bulkDataFlowConsumer.h bulkDataFlowConsumer.i \
		  bulkDataFlowProducer.h bulkDataFlowProducer.i \
		  bulkDataDistributerCb.h \
		  bulkDataDistributer.h bulkDataDistributer.i \
		  bulkDataDistributerImpl.h bulkDataDistributerImpl.i

LIBRARIES       = bulkDataCallback bulkDataDistributerLib
LIBRARIES_L     =

bulkDataCallback_OBJECTS   = bulkDataCallback
bulkDataCallback_LIBS      = ACSBulkDataError ACSBulkDataStatus archiveevents TAO_AV

bulkDataDistributerLib_OBJECTS   = bulkDataDistributerCb	
bulkDataDistributerLib_LIBS      = ACSBulkDataError ACSBulkDataStatus bulkDataDistributerStubs bulkDataStubs bulkDataSenderStubs bulkDataReceiverStubs baci ACSErrTypeCORBA TAO_Messaging

CDB_SCHEMAS = BulkDataReceiver BulkDataSender BulkDataDistributer

DEBUG=on 

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
