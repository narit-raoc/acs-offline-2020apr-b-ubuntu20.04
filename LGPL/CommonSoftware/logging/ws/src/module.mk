EXECUTABLES     = loggingService loggingClient
EXECUTABLES_L   = 

loggingService_OBJECTS = loggingService loggingACSLogFactory_i \
                         loggingACSStructuredPushSupplier \
                         loggingACSStructuredPushSupplierBin \
                         loggingACSStructuredPushSupplierXml loggingACSLog_i \
                         loggingHelper  loggingAcsLogServiceImpl

loggingService_LIBS = logging baselogging logging_idlStubs acsutil acscommonStubs loki log4cpp TAO_CosNotification_Serv

loggingClient_OBJECTS = loggingClient loggingHelper 
loggingClient_LIBS = logging baselogging logging_idlStubs acsutil acscommonStubs loki log4cpp TAO_CosNotification_Serv

INCLUDES        = 	logging.h loggingExport.h loggingBaseExport.h loggingXMLElement.h loggingXMLParser.h \
		  	loggingClient.h loggingLocalSyslog.h loggingRemoteSyslog.h loggingLocalFile.h \
			loggingCacheLogger.h \
			loggingBaseLog.h loggingStatistics.h loggingHandler.h loggingLogger.h loggingLoggable.h \
                        loggingLogTrace.h loggingACSLogger.h \
			loggingLogSvcHandler.h  loggingLogLevelDefinition.h \
			loggingLoggingTSSStorage.h loggingLoggingProxy.h loggingMACROS.h \
			loggingACEMACROS.h loggingGetLogger.h loggingGenericLogger.h loggingStdoutHandler.h \
			loggingAcsLogServiceImpl.h loggingStdoutlayout.h loggingXmlLayout.h\
            loggingACSCategory.h loggingACSLoggingEvent.h loggingLogThrottle.h \
            loggingACSRemoteAppender.h loggingLog4cpp.h loggingLog4cppMACROS.h \
			loggingLog4cppACEMACROS.h loggingACSHierarchyMaintainer.h loggingThrottleAlarmInterface.h \
			loggingStopWatch.h loggingExecuteWithLogger.h loggingExecuteWithLogger.i
INCLUDES_L      = loggingService.h \
		  loggingACSStructuredPushSupplier.h \
		  loggingACSStructuredPushSupplierXml.h \
          loggingACSStructuredPushSupplierBin.h \
          loggingACSLogFactory_i.h loggingACSLog_i.h \
		  loggingHelper.h logging ACSHierarchyMaintainer.h 

LIBRARIES       = baselogging logging 
LIBRARIES_L     =

baselogging_OBJECTS =	loggingBaseLog loggingStatistics loggingHandler loggingLogger \
                        loggingLogTrace loggingGetLogger loggingGenericLogger \
                        loggingStdoutHandler \
                        loggingLoggingProxy loggingLocalFile loggingXMLElement \
                        loggingRemoteSyslog loggingLocalSyslog loggingACSLogger loggingLogSvcHandler  \
						loggingLogThrottle \
						loggingACSLoggingEvent loggingXmlLayout \
						loggingStdoutlayout loggingACSCategory \
						loggingACSRemoteAppender loggingACSHierarchyMaintainer \
						loggingLog4cpp loggingStopWatch loggingLogLevelDefinition loggingXMLParser

baselogging_LIBS =    acsutil loki log4cpp logging_idlStubs
baselogging_LDFLAGS = -ggdb
baselogging_CFLAGS  = -DLOGGINGBASE_BUILD_DLL
logging_OBJECTS 	=	logging loggingLoggable loggingXMLElement loggingXMLParser \
						loggingLocalSyslog loggingRemoteSyslog loggingLocalFile \
						loggingLoggingTSSStorage loggingLogLevelDefinition \
						loggingLoggingProxy loggingACSLogger loggingLogSvcHandler  \
						loggingLogThrottle \
						loggingACSLoggingEvent loggingXmlLayout \
						loggingStdoutlayout loggingACSCategory \
						loggingACSRemoteAppender loggingACSHierarchyMaintainer \
						loggingLog4cpp loggingStopWatch
						
logging_LIBS    =	logging_idlStubs acsutil acscommonStubs baselogging log4cpp pthread rt
logging_LDFLAGS =	-ggdb
logging_CFLAGS  = -DLOGGING_BUILD_DLL

INSTALL_FILES = ../idl/loggingMI.xsd

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
