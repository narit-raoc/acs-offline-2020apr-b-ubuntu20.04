DEBUG=on
JARFILES = xmljbind
xmljbind_DIRS = alma
xmljbind_EXTRAS = 
xmljbind_ENDORSED = on
xmljbind_JARS:=jACSUtil castor
xmljbind_DEPS:=$(mod_jacsutil_name)_jACSUtil_jar
$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
