MAKE_NOIFR_CHECK = on # todo check this

IDL_FILES = acscommon
TAO_IDLFLAGS = -GT
IDL_EXTENSIONS = S_T.h S_T.cpp
acscommonStubs_LIBS = acserrStubs

XSDBIND = commontypes

DEBUG = on

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
