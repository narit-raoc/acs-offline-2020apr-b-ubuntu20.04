INCLUDES        = cdbField.h cdb.h cdb.i cdbData_Types.h cdbINIReader.h cdbExport.h cdbDALaccess.h cdbDAOImpl.h cdbDAONode.h cdbDAOProxy.h

LIBRARIES       = cdb

cdb_OBJECTS   = cdbField cdb cdbIMDB cdbINIReader cdbDALaccess cdbDAOImpl cdbDAONode cdbDAOProxy
cdb_LIBS      = cdbDALStubs ACSErrTypeCommon ACSErrTypeCORBA cdbErrType acscomponentStubs logging expat
cdb_CFLAGS    = -DCDB_BUILD_DLL

SCRIPTS         = cdbjDAL cdbjDALShutdown cdbjDALClearCache cdbRead cdbWrite cdbTATPrologue cdbTATEpilogue cdbSetDefaultComponent cdbChangeComponentDeployment cdbAddImplLang.py 

#XERCES_JAR:=$(shell searchFile lib/endorsed/xercesImpl.jar)
#$(if $(filter #error#,$(XERCES_JAR)),$(error "xercesImpl.jar was not found."),)
#XMLPARSER_JAR:=$(shell searchFile lib/endorsed/XmlParserAPIs.jar)
#$(if $(filter #error#,$(XMLPARSER_JAR)),$(error "XmlParser.jar was not found."),)
#CLASSPATH:=$(CLASSPATH):$(XERCES_JAR)

DEBUG=on

JARFILES:=CDB
CDB_DIRS:=com
CDB_JARS:=jACSUtil acsjlog

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
