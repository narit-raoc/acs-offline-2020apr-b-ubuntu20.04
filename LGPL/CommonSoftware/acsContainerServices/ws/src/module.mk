INCLUDES        = acsComponentListener.h acsContainerServices.h acsContainerServices.i \
				  acsComponentStateManager.h acsComponentSmartPtr.h

LIBRARIES       = acsComponentListener acsContainerServices 
LIBRARIES_L     = 

acsContainerServices_OBJECTS   = acsContainerServices 
acsContainerServices_LIBS = acsErrTypeContainerServices acsErrTypeLifeCycle acsThread acsComponentListener
 
acsComponentListener_OBJECTS = acsComponentListener
acsComponentListener_LIBS = logging

ACSERRDEF = acsErrTypeContainerServices acsErrTypeLifeCycle JavaContainerError

JARFILES=acsContainerServices 

acsContainerServices_DIRS = alma
acsContainerServices_EXTRAS = alma/acs/nc/sm/generated/EventSubscriberSCXML.xml
acsContainerServices_JLIBS = acsErrTypeLifeCycle JavaContainerError

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
