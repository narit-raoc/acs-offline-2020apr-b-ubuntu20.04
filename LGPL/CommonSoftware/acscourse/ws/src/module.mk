DEBUG=on

ACSERRDEF = ACSErrTypeACSCourse

JARFILES =              acscourseImpl

acscourseImpl_DIRS   =  alma
acscourseImpl_EXTRAS =
acscourseImpl_JLIBS = acscourseMount

XSDBIND = acscourseEntities
XSDBIND_INCLUDE = systementities   

COMPONENT_HELPERS=on

XML_IDL="MyXmlConfigData=alma.acscourse.xmlbinding.myxmlconfigdata.MyXmlConfigData"

INCLUDES =	acscourseMount1Impl.h \
	        acscourseMount2Impl.h acscourseMount2LoopImpl.h \
		acscourseMount3Impl.h acscourseMount4Impl.h acscourseMount5Impl.h

LIBRARIES =	acscourseMount1Impl acscourseMount2Impl acscourseMount2LoopImpl acscourseMount3Impl acscourseMount4Impl acscourseMount5Impl

acscourseMount1Impl_OBJECTS	= acscourseMount1Impl
acscourseMount1Impl_LIBS	= acscourseMountStubs xmlentityStubs ACSErrTypeACSCourse acscomponent archiveevents

acscourseMount2Impl_OBJECTS	= acscourseMount2Impl acscourseMount2ImplDLL
acscourseMount2Impl_LIBS	= acscourseMountStubs xmlentityStubs ACSErrTypeACSCourse acscomponent baci archiveevents

acscourseMount2LoopImpl_OBJECTS	= acscourseMount2LoopImpl acscourseMount2Impl
acscourseMount2LoopImpl_LIBS	= acscourseMountStubs xmlentityStubs ACSErrTypeACSCourse acscomponent baci archiveevents

acscourseMount3Impl_OBJECTS	= acscourseMount3Impl 
acscourseMount3Impl_LIBS	= acscourseMountStubs xmlentityStubs ACSErrTypeACSCourse acscomponent baci archiveevents

acscourseMount4Impl_OBJECTS	= acscourseMount4Impl
acscourseMount4Impl_LIBS	= acscourseMountStubs xmlentityStubs ACSErrTypeACSCourse acscomponent baci archiveevents

acscourseMount5Impl_OBJECTS	= acscourseMount5Impl
acscourseMount5Impl_LIBS	= acscourseMountStubs xmlentityStubs acsnc ACSErrTypeACSCourse acscomponent baci archiveevents

CDB_SCHEMAS = ACSCourseMount ACSCourseMountRW

IDL_FILES = 	acscourseMount
USER_IDL = 
acscourseMountStubs_LIBS = baciStubs ACSErrTypeACSCourse ACSErrTypeCommon xmlentityStubs

PY_SCRIPTS = mountSimple mountCallback acscourseMountSupplier acscourseMountConsumer

PY_PACKAGES = ACSCOURSE_MOUNTImpl

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
