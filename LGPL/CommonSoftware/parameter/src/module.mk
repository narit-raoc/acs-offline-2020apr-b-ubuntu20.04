INCLUDES        = ParameterSet.h ParamSetDef.h BoolParam.h IntParam.h IntArrayParam.h DoubleArrayParam.h DoubleParam.h StringParam.h StringArrayParam.h ParamStrX.h IntParamDef.h IntArrayParamDef.h DoubleParamDef.h DoubleArrayParamDef.h StringParamDef.h StringArrayParamDef.h BoolParamDef.h ParamDef.h QuantityParam.h Param.h InMemoryXmlData.h parameterConstants.h

LIBRARIES       = acsXercesUtilities parameter
LIBRARIES_L     =

parameter_OBJECTS   = ParameterSet ParamSetDef ParamDef IntParamDef StringParamDef \
                         DoubleParamDef BoolParamDef IntArrayParamDef \
                         DoubleArrayParamDef StringArrayParamDef Param QuantityParam IntParam IntArrayParam \
                         DoubleArrayParam DoubleParam StringParam StringArrayParam BoolParam 
parameter_LIBS := logging xerces-c acsXercesUtilities

acsXercesUtilities_OBJECTS = acsDOMErrorHandler 
acsXercesUtilities_LIBS := loki logging xerces-c

INSTALL_FILES = ../idl/psetdef.xsd ../idl/pset.xsd

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
