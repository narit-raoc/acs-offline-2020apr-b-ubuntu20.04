INCLUDES        = acsThreadBase.h acsThread.h acsThreadManager.h acsThreadManager.i acsThreadExport.h

LIBRARIES       = acsThread
LIBRARIES_L     =

acsThread_OBJECTS   = acsThread acsThreadBase
acsThread_LIBS  = logging acsthreadErrType ACSErrTypeCommon
acsThread_CFLAGS  = -DACS_THREAD_BUILD_DLL

ACSERRDEF = acsthreadErrType

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
