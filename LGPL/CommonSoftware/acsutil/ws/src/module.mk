INCLUDES        = acsutil.h acsutilWildcard.h acsutilExport.h acsutilThreadInit.h acsutilFindFile.h acsutilLL.h acsutilTempFile.h acsutilPorts.h \
		  acsutilAnyAide.h acsutilTimeStamp.h acsutilORBHelper.h
INCLUDES_L      = 

LIBRARIES       = acsutil
LIBRARIES_L     =

acsutil_OBJECTS	= acsutilWildcard acsutilFindFile acsutilLL acsutilTempFile acsutilPortsImpl acsutilAnyAide acsutilTimeStamp acsutilORBHelper
acsutil_LIBS    = acscommonStubs

SCRIPTS         = acsutilTATPrologue acsutilTATEpilogue acsutilTATTestRunner acsutilProfiler acsutilDiffTrap acsutilRedo \
		  acsutilBlock acsutilAwaitContainerStart 
SCRIPTS_L       = 

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
