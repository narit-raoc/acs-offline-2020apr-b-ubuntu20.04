PY_SCRIPTS         = generateXsdPythonBinding
PY_PACKAGES        = xmlpybind
xmlpybind_DEPS:=$(MODPATH)/src/xmlpybind

$(MODRULE)all: $(MODPATH) $(MODDEP) $(MODPATH)/src/xmlpybind
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)$(if $(wildcard $(MODPATH)/src/xmlpybind),rm -rf $(MODPATH)/src/xmlpybind,)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODPATH)/src/xmlpybind: $(MODPATH) $(call findDep,pyxbgen,pyscr,bin,0)
	@echo "genearting schemata..."
	$(call findDep,pyxbgen,pyscr,bin,0) --module-prefix=xmlpybind -u $(MODPATH)/../xmljbind/src/alma/tools/entitybuilder/EntitybuilderSettings.xsd -m EntitybuilderSettings --binding-root=$(MODPATH)/src
