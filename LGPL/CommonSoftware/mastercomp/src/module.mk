CDB_SCHEMAS = MasterComponent

IDL_FILES = mastercomp_if
IDL_TAO_FLAGS =
USER_IDL =

mastercomp_ifStubs_LIBS = baciStubs ACSErrTypeCommonStubs

JARFILES= mastercomp

mastercomp_DIRS=alma
mastercomp_JLIBS=mastercomp_if

DEBUG=on

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
