SCRIPTS         = acscommandcenter accStarter accStopper accEnableVars accStarter2
SCRIPTS_L       =

XSDBIND = AcsCommandCenterEntities
XSDBIND_INCLUDE = systementities

XML_IDL = "AcsCommandCenterProject=alma.entity.xmlbinding.acscommandcenterproject.AcsCommandCenterProject;AcsCommandCenterTools=alma.entity.xmlbinding.acscommandcentertools.AcsCommandCenterTools"

JARFILES= acscommandcenter
acscommandcenter_DIRS= alma
acscommandcenter_EXTRAS= alma/acs/commandcenter/resources/*.gif \
                         alma/acs/commandcenter/resources/*.jpg \
                         AcsCommandCenterTools.xml \
                         AcsCommandCenterBuiltinTools.xml \
                         VERSION
acscommandcenter_JLIBS=AcsCommandCenterEntities
                         
DEBUG= on

INSTALL_FILES = ../lib/JavaOnlyAcsConfig.jar \
					 ../lib/trilead-ssh2-build213.jar \
					 ../lib/AcsCommandCenterHelp.jar \
					 ../lib/xtmodcoll-3.0.jar

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
