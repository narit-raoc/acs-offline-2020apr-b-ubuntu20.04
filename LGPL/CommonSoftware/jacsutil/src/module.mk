INSTALL_FILES = ../lib/jhall-2.0_05.jar

DEBUG = on

JARFILES:=jACSUtil
jACSUtil_DIRS:=com alma
jACSUtil_EXTRAS:=alma/acs/jhelpgen/content/*.gif alma/acs/classloading/*.txt
jACSUtil_JARS:=junit-dep-4.10 commons-lang-2.5 hibernate-core-5.3.7.Final hibernate-jpa-2.1-api-1.0.2.Final

SCRIPTS         = acsExtractJavaSources acsJarPackageInfo acsJarsearch acsJarSignInfo acsJarUnsign acsJavaHelp

CLASSPATH:=$(JAVA_HOME)/lib/tools.jar$(PATH_SEP)$(CLASSPATH)$(PATH_SEP)$(MODPATH)

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . copying jacorb property files"
	$(AT)cp $(MODPATH)/config/orb.properties $(ACSDATA)/config
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
