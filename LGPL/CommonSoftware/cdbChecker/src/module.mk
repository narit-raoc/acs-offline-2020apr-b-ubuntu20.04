JARFILES=cdbChecker 

cdbChecker_DIRS = cl alma
cdbChecker_ENDORSED = on

CLASSPATH:=$(JAVA_HOME)/lib/tools.jar$(PATH_SEP)$(CLASSPATH)

DEBUG = on 

INSTALL_FILES= 	../lib/java-getopt-1.0.12.jar ../config/reqSchemas.xml \
		../config/reqSchemas.xsd

SCRIPTS         = cdbChecker

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
