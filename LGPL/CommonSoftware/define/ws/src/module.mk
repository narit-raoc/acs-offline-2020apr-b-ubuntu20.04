DEBUG = on

COMPONENT_HELPERS=on

XSDBIND = systementities

XML_IDL = "IdentifierRange=alma.archive.range.IdentifierRange"

ACSERRDEF = ArchiveIdentifierError

IDL_FILES = xmlentity archive_xmlstore_if 
IDL_TAO_FLAGS =
USER_IDL =

archive_xmlstore_ifStubs_LIBS = acscommonStubs acscomponentStubs xmlentityStubs

LIBRARIES       = xmlentity archive_xmlstore_if
LIBRARIES_L     =

xmlentity_OBJECTS           =
xmlentity_LIBS := xmlentityStubs

archive_xmlstore_if_OBJECTS = 
archive_xmlstore_if_LIBS := archive_xmlstore_ifStubs
archive_xmlstore_if_EXTRAS = -C ../idl/IdentifierRange.xsd

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
