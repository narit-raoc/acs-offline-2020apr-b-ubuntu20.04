INCLUDES      = baciCharacteristicModelImpl.h baciCharacteristicComponentImpl.h \
		baciAlarm_T.h baciAlarm_T.i \
		baciAlarmBoolean.h baciAlarmBooleanSeq.h \
		baciPcommonImpl_T.h baciPcommonImpl_T.i baciPcontImpl_T.h baciPcontImpl_T.i \
		baciROcommonImpl_T.h baciROcommonImpl_T.i baciROcontImpl_T.h \
                baciROcontImpl_T.i \
                baciROdiscImpl_T.h baciROdiscImpl_T.i \
	        baciROSeqCommonImpl_T.h baciROSeqContImpl_T.h baciROSeqContImpl_T.i \
		baciRWcommonImpl_T.h baciRWcommonImpl_T.i baciRWcontImpl_T.h \
                baciRWcontImpl_T.i \
                baciRWdiscImpl_T.h baciRWdiscImpl_T.i \
		baciRWSeqContImpl_T.h baciRWSeqContImpl_T.i \
		baciCORBAMem.h \
		baciDevIO.h \
		baciDevIOMem.h\
		baciExport.h \
		baciValue.h baciValue.i baciThread.h baciTypes.h \
		baci.h baciTime.h \
		baciCORBA.h baciDB.h \
		baciRecovery.h baciRecoverableObject.h \
		baciEvent.h baciCDBPropertySet.h  \
		baciROdouble.h baciRWdouble.h \
		baciROfloat.h baciRWfloat.h \
		baciROlongLong.h baciRWlongLong.h \
		baciROuLongLong.h baciRWuLongLong.h \
		baciROlong.h baciRWlong.h \
		baciROuLong.h baciRWuLong.h \
		baciROboolean.h baciRWboolean.h \
		baciPpatternImpl.h baciROpattern.h baciRWpattern.h \
		baciROstring.h baciRWstring.h \
		baciROdoubleSeq.h baciROfloatSeq.h baciROlongSeq.h baciROuLongSeq.h baciRObooleanSeq.h\
		baciRWdoubleSeq.h baciRWfloatSeq.h baciRWlongSeq.h baciRWuLongSeq.h baciRWbooleanSeq.h\
		baciMonitor_T.h baciMonitor_T.i \
		baciRegistrar.h baciRegistrar.i \
		baciError.h \
		baciPropertyImpl.h\
		baciSmartPropertyPointer.h baciSmartPropertyPointer.i \
		baciSmartServantPointer.h baciSmartServantPointer.i \
		baciBACIAction.h baciBACICallback.h baciBACIMonitor.h baciUtil.h baciBACIProperty.h baciBACIComponent.h \
		baciAlarmSystemMonitorBoolean.h baciAlarmSystemMonitorBooleanSeq.h \
		baciAlarmSystemMonitorCont_T.h baciAlarmSystemMonitorCont_T.i \
		baciAlarmSystemMonitorDisc_T.h baciAlarmSystemMonitorDisc_T.i \
		baciAlarmSystemMonitorSeqCont_T.h  baciAlarmSystemMonitorSeqCont_T.i \
		baciAlarmSystemMonitorSeqDisc_T.h baciAlarmSystemMonitorSeqDisc_T.i \
		baciAlarmSystemMonitorPattern.h \
	    baciAlarmSystemMonitorBase.h    baciAlarmSystemMonitor_T.h  baciAlarmSystemMonitor_T.i

LIBRARIES = baci
LIBRARIES_L =

baci_OBJECTS =  baciValue baciBACIAction baciBACICallback baciBACIMonitor baciUtil baciBACIProperty baciBACIComponent \
		baciCharacteristicModelImpl baciCharacteristicComponentImpl\
		baciError \
		baci baciTime baciDB \
		baciRecovery baciCORBA baciDLL \
		baciEvent baciCDBPropertySet \
		baciROdouble baciRWdouble baciCheckDevIOValue \
		baciROfloat baciRWfloat \
		baciROlongLong baciRWlongLong \
		baciROuLongLong baciRWuLongLong \
		baciROlong baciRWlong \
		baciROuLong baciRWuLong \
		baciROboolean baciRWboolean baciAlarmBoolean baciAlarmSystemMonitorBoolean \
		baciAlarmSystemMonitorBase \
		baciPpatternImpl baciROpattern baciRWpattern baciAlarmPattern baciAlarmSystemMonitorPattern\
	        baciROstring baciRWstring \
	    baciRObooleanSeq baciAlarmBooleanSeq baciAlarmSystemMonitorBooleanSeq \
		baciROdoubleSeq baciROfloatSeq baciROlongSeq baciROuLongSeq \
		baciRWdoubleSeq baciRWfloatSeq baciRWlongSeq baciRWuLongSeq baciRWbooleanSeq
baci_LIBS = baciErrTypeProperty baciErrTypeDevIO acsThread acsErrTypeLifeCycle alSysSource acsAlSysSource \
			acsErrTypeAlarmSourceFactory ACSErrTypeMonitor PatternAlarmCleared PatternAlarmTriggered \
			acsutil cdb logging acscomponent recovery acserr baciStubs archiveevents TAO_CosProperty_Serv
baci_CFLAGS = -DBACI_BUILD_DLL

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
