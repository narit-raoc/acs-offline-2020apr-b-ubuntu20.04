INCLUDES = acstestcompTimingExplorerImpl.h acstestcompErrorExplorerImpl.h

LIBRARIES = acstestcompTimingExplorer acstestcompErrorExplorer

acstestcompTimingExplorer_OBJECTS =  acstestcompTimingExplorerImpl
acstestcompTimingExplorer_LDFLAGS =
acstestcompTimingExplorer_LIBS    = acstestcompStubs logging acsThread acscomponent archiveevents

acstestcompErrorExplorer_OBJECTS =  acstestcompErrorExplorerImpl
acstestcompErrorExplorer_LDFLAGS =
acstestcompErrorExplorer_LIBS    = acstestcompStubs baci

CDB_SCHEMAS = ErrorExplorer 

IDL_FILES = acstestcomp
IDL_TAO_FLAGS =
USER_IDL =

acstestcompStubs_LIBS = acscomponentStubs baciStubs acserrStubs

DEBUG=on

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
