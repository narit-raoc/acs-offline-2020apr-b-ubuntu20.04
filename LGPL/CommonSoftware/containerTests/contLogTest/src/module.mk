COMPONENTS_JARFILES = contLogTestComp

contLogTestComp_DIRS = \
	alma/contLogTest/TestLogLevelsCompImpl
contLogTestComp_JLIBS = contLogTest_IF

JARFILES = contLogTestClient

contLogTestClient_DIRS = \
	alma/contLogTest/client
contLogTestClient_JLIBS = contLogTest_IF

DEBUG = on
COMPONENT_HELPERS=on

ACSLOGTSDEF = typeSafeLogs

INCLUDES =	contLogTestImpl.h
INCLUDES_L      = 

LIBRARIES =	contLogTestImpl
LIBRARIES_L     =

contLogTestImpl_OBJECTS = contLogTestImpl
contLogTestImpl_LIBS    = contLogTest_IFStubs ACSErrTypeCommon acscomponent archiveevents

PY_PACKAGES        = pyContLogTest
PY_PACKAGES_L      =

IDL_FILES = contLogTest_IF
IDL_TAO_FLAGS =
USER_IDL =

contLogTest_IFStubs_LIBS = acscomponentStubs ACSErrTypeCommonStubs

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
