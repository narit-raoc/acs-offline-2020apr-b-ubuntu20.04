INCLUDES        =
INCLUDES_L      = CounterSupplierImpl.h

LIBRARIES       = CounterConsumerImpl CounterSupplierImpl 
LIBRARIES_L     =

CounterSupplierImpl_OBJECTS   = CounterSupplierImpl
CounterSupplierImpl_LIBS      = contNcTest_IFStubs ACSErrTypeCommon acsnc acscomponent

CounterConsumerImpl_OBJECTS   = CounterConsumerImpl
CounterConsumerImpl_LIBS      = contNcTest_IFStubs ACSErrTypeCommon acsnc acscomponent

PY_SCRIPTS         = contNcTestCounterSupplier contNcTestCounterConsumer
PY_SCRIPTS_L       =

PY_PACKAGES        = pyCounterTest
PY_PACKAGES_L      =

IDL_FILES = contNcTest_IF
IDL_TAO_FLAGS =
USER_IDL =

contNcTest_IFStubs_LIBS = acscomponentStubs ACSErrTypeCommonStubs

COMPONENTS_JARFILES = counterSupplierComp counterConsumerComp

counterSupplierComp_DIRS = alma/COUNTER/CounterSupplierImpl
counterSupplierComp_JLIBS = contNcTest_IF

counterConsumerComp_DIRS = alma/COUNTER/CounterConsumerImpl
counterConsumerComp_JLIBS = contNcTest_IF

DEBUG=on
COMPONENT_HELPERS=on

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
