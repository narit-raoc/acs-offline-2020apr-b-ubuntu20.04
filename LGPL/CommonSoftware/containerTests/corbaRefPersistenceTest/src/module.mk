IDL_FILES = HelloWorld componentGetter
IDL_TAO_FLAGS =
USER_IDL =

HelloWorldStubs_LIBS = acscomponentStubs ACSErrTypeCommonStubs

componentGetterStubs_LIBS = acscomponentStubs

DEBUG=On 

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
