SCRIPTS         = alarmService alarmServiceShutdown
SCRIPTS_L       =

ACSERRDEF = acsErrTypeAlarmSourceFactory

CDB_SCHEMAS = 	AcsAlarmSystem \
				acsalarm-fault-family \
				acsalarm-categories \
				acsalarm-alarmservice

IDL_FILES = AcsAlarmSystem
IDL_TAO_FLAGS =
USER_IDL =

AcsAlarmSystemStubs_LIBS = ACSErrTypeCommonStubs acserrStubs

JARFILES= acsAlarmService

acsAlarmService_DIRS=alma
acsAlarmService_JLIBS=AcsAlarmSystem acsalarmSchemaBindings

DEBUG=on 

XSDBIND=acsalarmSchemaBindings

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
