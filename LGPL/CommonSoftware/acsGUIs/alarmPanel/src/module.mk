SCRIPTS         = alarmPanel
SCRIPTS_L       =

JARFILES= alarmPanel

alarmPanel_DIRS= alma
alarmPanel_EXTRAS= alma/acsplugins/alarmsystem/gui/resources \
					alma/acsplugins/alarmsystem/gui/sound/resources

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
