DEBUG = on

JARFILES = lc

lc_DIRS = com alma

SCRIPTS         = jlog acsLogAssistant
SCRIPTS_L       =

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo "== Adding extra files to lc.jar"
	$(AT)cd $(MODPATH)/src/lc.jar.extras; jar -uf $(MODPATH)/lib/lc.jar *
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
