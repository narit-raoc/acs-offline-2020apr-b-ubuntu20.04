JARFILES = oe

oe_DIRS = si
oe_EXTRAS = device.gif domain.gif invocation.gif property.gif type.gif
oe_JLIBS=objexpErrType

ACSERRDEF = objexpErrType

SCRIPTS         = objexp objexpNonSticky objexpSticky
SCRIPTS_L       =

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
