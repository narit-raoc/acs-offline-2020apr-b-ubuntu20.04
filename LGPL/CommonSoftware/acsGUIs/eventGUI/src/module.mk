SCRIPTS         = eventGUI
SCRIPTS_L       =

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)cd $(MODPATH)/src; ant init createJarsPlugin pde-build; cd -
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)cd $(INSTDIR)/lib; \
	rm -rf ACS_eventGUI; \
	unzip -o $(MODPATH)/object/headlessTemp/I.eventGUIBuild/eventGUIBuild-linux.gtk.$(ZIP_SUFFIX).zip;
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)cd $(MODPATH)/src; ant clean; cd -
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
