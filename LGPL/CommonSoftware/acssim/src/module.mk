SCRIPTS         = recordEvents
SCRIPTS_L       = acssimPydoc

PY_PACKAGES        = Acssim
PY_PACKAGES_L      =

IDL_FILES = Simulator
IDL_TAO_FLAGS =
USER_IDL =

SimulatorStubs_LIBS = acscomponentStubs

CDB_SCHEMAS = SimulatedComponent

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
