INCLUDES        = enumpropRWImpl.h enumpropRWImpl.i \
		  enumpropROImpl.h enumpropROImpl.i \
		  enumpropAlarm.h enumpropAlarm.i \
		  enumpropAlarmSystemMonitorEnumProp.h enumpropAlarmSystemMonitorEnumProp.i

DEBUG=on

IDL_FILES = enumpropMACRO_included enumpropMACRO enumpropStd
enumpropStdStubs_LIBS := baciStubs

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
