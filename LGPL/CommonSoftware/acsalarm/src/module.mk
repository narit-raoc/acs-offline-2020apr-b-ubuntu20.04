INCLUDES        = 	ACSAlarmSystemInterfaceFactory.h \
					ACSAlarmSystemInterfaceProxy.h \
					FaultState.h \
					faultStateConstants.h \
			      AlarmSystemInterfaceFactory.h Properties.h Timestamp.h \
					AlarmSystemInterface.h ASIMessage.h ASIConfiguration.h asiConfigurationConstants.h \
					AlarmSourceThread.h AlarmSource.h AlarmSourceImpl.h AlarmToQueue.h AlarmsMap.h

LIBRARIES       = alSysSource acsAlSysSource alarmSource 
LIBRARIES_L     = 

alSysSource_OBJECTS = FaultState \
					  AlarmSystemInterface \
					  AlarmSystemInterfaceFactory \
					  Properties \
					  Timestamp \
					  ASIMessage \
					  ASIConfiguration
alSysSource_LIBS = acsThread logging loki ACE

acsAlSysSource_OBJECTS   = ACSAlarmSystemInterfaceFactory ACSAlarmSystemInterfaceProxy ConfigPropertyGetter 
acsAlSysSource_LIBS = maciStubs alSysSource acsutil expat maciErrType loki  cdbErrType acsErrTypeAlarmSourceFactory cdb

alarmSource_OBJECTS = AlarmSourceThread AlarmToQueue AlarmsMap AlarmSourceImpl
alarmSource_LIBS = alSysSource acsAlSysSource 

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
