EXECUTABLES     = $(PROG) maciContainer maciManagerShutdown maciContainerShutdown maciReleaseComponent maciContainerLogLevel maciContainerLogStatsConfiguration maciComponentRefDump
EXECUTABLES_L   = 

maciContainer_OBJECTS   = maciContainer 
maciContainer_LIBS = maci acsContainerServices acsComponentListener logging TAO_CosNotification_MC_Ext TAO_DynamicInterface

maciManagerShutdown_OBJECTS = maciManagerShutdown
maciManagerShutdown_LIBS = maci

maciContainerShutdown_OBJECTS = maciContainerShutdown
maciContainerShutdown_LIBS = maci

maciReleaseComponent_OBJECTS = maciReleaseComponent
maciReleaseComponent_LIBS = maci

maciContainerLogLevel_OBJECTS = maciContainerLogLevel
maciContainerLogLevel_LIBS = maci

maciContainerLogStatsConfiguration_OBJECTS = maciContainerLogStatsConfiguration
maciContainerLogStatsConfiguration_LIBS = maci

maciComponentRefDump_OBJECTS = maciComponentRefDump
maciComponentRefDump_LIBS= maciClient

INCLUDES	= maciHelper.h maciSimpleClient.h \
              maciSimpleClientThreadHook.h maciExport.h maciContainerServices.h \
              maciContainerImpl.h maciContainerThreadHook.h \
		      maciACSComponentDefines.h maciPropertyDefines.h \
              maciLibraryManager.h maciRegistrar.h maciComponentStateManager.h\
		      maciRegistrar.i maciClientExport.h maciLogThrottleAlarmImpl.h
INCLUDES_L	= maciContainer.h maciServantManager.h \
		      maciORBTask.h 

LIBRARIES       = maci maciClient
LIBRARIES_L     =

maci_OBJECTS   = maciLibraryManager maciORBTask  maciContainerThreadHook \
		 maciServantManager maciContainerServices maciContainerImpl \
		 maciHelper maciComponentStateManager maciLogThrottleAlarmImpl
maci_LIBS = acsContainerServices maciErrType acsErrTypeContainerServices maciStubs acsAlSysSource acsErrTypeAlarmSourceFactory AcsContainerLogLTS alarmSource acscomponent acsQoS archiveevents
maci_LDFLAGS = -ggdb
maci_CFLAGS = -DMACI_BUILD_DLL

maciClient_OBJECTS =  maciSimpleClient maciSimpleClientThreadHook
maciClient_LIBS    =  baci maci maciErrType
maciClient_CFLAGS = -DMACICLIENT_BUILD_DLL

DEBUG=on

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
