PY_SCRIPTS         = acspyexmplFridgeNCClient \
		     acspyexmplFridgeNCConsumer \
		     acspyexmplFridgeNCSupplier \
		     acspyexmplHelloWorldError \
		     acspyexmplMountCallback \
		     acspyexmplMountClient \
		     acspyexmplDynamicHelloWorld \
		     acspyexmplTimeoutHandler \
		     acspyexmplClientErrorComponent
PY_SCRIPTS_L       =

PY_MODULES         = acspyexmplSimulatorExtras
PY_MODULES_L       =

PY_PACKAGES        = demoImpl acsexmplLampImpl acsexmplErrorComponentImpl
PY_PACKAGES_L      =

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
