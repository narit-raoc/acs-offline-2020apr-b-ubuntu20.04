INCLUDES        = TypeSafeLog.h

SCRIPTS         = loggingtsGenPython loggingtsGenH loggingtsGenCpp loggingtsGenJava loggingtsGenCheckXML
SCRIPTS_L       =

DEBUG=on

INSTALL_FILES = ../config/LTS2Py.xslt ../config/LTS2Cpp.xslt ../config/LTS2H.xslt ../config/LTS2Java.xslt ../idl/ACSLogTS.xsd

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
