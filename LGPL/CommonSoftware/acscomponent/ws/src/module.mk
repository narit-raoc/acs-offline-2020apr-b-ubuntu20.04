INCLUDES        = acscomponentImpl.h 

LIBRARIES       = acscomponent
LIBRARIES_L     =

acscomponent_OBJECTS   = acscomponentImpl 
acscomponent_LIBS      = acscomponentStubs acsErrTypeLifeCycle acsErrTypeComponent acsThread

ACSERRDEF = acsErrTypeComponent

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
