SCRIPTS         = nsStatistics
SCRIPTS_L       =

IDL_FILES=NotifyServiceStatisticsComponent 
TAO_IDLFLAGS =
USER_IDL =

NotifyServiceStatisticsComponentStubs_LIBS=acscomponentStubs

JARFILES=nsStatisticsService NotifyServiceStatisticsComponentImpl 
nsStatisticsService_DIRS=alma/acs/nsstatistics \
                         alma/acs/service
                         
NotifyServiceStatisticsComponentImpl_DIRS=alma/acs/statistics
NotifyServiceStatisticsComponentImpl_JLIBS=nsStatisticsService

DEBUG=on

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
