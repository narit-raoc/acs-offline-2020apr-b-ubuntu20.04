DEBUG = on

JARFILES = acsjlog slf4j-acs

acsjlog_DIRS:=alma
acsjlog_EXTRAS:=almalogging.properties
acsjlog_JARS:=jACSUtil maciSchemaBindings log4j-1.2.17 castor commons-math-2.1 commons-logging-1.2 junit-dep-4.10# slf4j-acs

slf4j-acs_DIRS:=org
slf4j-acs_JARS:=slf4j-api-1.7.23

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
