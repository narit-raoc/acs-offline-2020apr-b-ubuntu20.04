IDL_FILES = TMCDBCOMMON_IDL MonitorCollector MCtestComponent CollectorListStatus MonitorArchiverIF 
TAO_IDLFLAGS =
USER_IDL =

TMCDBCOMMON_IDLStubs_LIBS = acscommonStubs

MonitorCollectorStubs_LIBS = acscommonStubs acscomponentStubs TMCDBCOMMON_IDLStubs MonitorErr

MCtestComponentStubs_LIBS = acscommonStubs baciStubs

CollectorListStatusStubs_LIBS = baciStubs enumpropMACROStubs

MonitorArchiverIFStubs_LIBS = acscomponentStubs CollectorListStatusStubs MonitorErr

ACSERRDEF= MonitorErr DAOErrType

MonitorErrStubs_LIBS = acserr
DAOErrTypeStubs_LIBS = acserr

JARFILES=MonitoringDAO_IF 

MonitoringDAO_IF_DIRS=alma

DEBUG= on 

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
