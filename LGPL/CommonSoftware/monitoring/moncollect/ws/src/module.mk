INCLUDES        = MonitorCollectorImpl.h MonitorComponent.h MonitorPoint.h MonitorPoint.i

LIBRARIES       = MonitorCollector
LIBRARIES_L     =

MonitorCollector_OBJECTS   = MonitorCollectorImpl MonitorComponent MonitorPoint
MonitorCollector_LIBS = baci TMCDBCOMMON_IDLStubs MonitorCollectorStubs MonitorArchiverIFStubs MonitorErr CollectorListStatusStubs acscommonStubs enumpropMACROStubs TAO_DynamicInterface

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
