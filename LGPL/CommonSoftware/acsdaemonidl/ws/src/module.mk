ACSERRDEF = acsdaemonErrType

IDL_FILES = acsdaemon
IDL_TAO_FLAGS =
USER_IDL =

acsdaemonStubs_LIBS = acsdaemonErrTypeStubs ACSErrTypeCommonStubs acscomponentStubs maciErrTypeStubs acscommonStubs

DEBUG=on 

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
