USER_CFLAGS =  -g -O0
USER_LIB    =

DEBUG=on

EXECUTABLES     = acsncDelChannelsInNameS
EXECUTABLES_L   =

acsncDelChannelsInNameS_OBJECTS = acsncDelChannelsInNameS
acsncDelChannelsInNameS_LIBS = acsutil logging

INCLUDES = 	acsncHelper.h acsncSupplier.h acsncConsumer.h \
            acsncORBHelper.h \
            acsncSimpleSupplier.i acsncSimpleSupplier.h \
            acsncSimpleConsumer.i acsncSimpleConsumer.h \
            acsncRTSupplier.i acsncRTSupplier.h \
            acsncArchiveConsumer.h \
            acsncCDBProperties.h \
            acsncReconnectionCallback.h \
            acsncCircularQueue.h acsncBlockingQueue.h acsncBlockingQueue.i
INCLUDES_L	= 

LIBRARIES       = acsnc
LIBRARIES_L     =

acsnc_OBJECTS  = acsncCDBProperties \
                 acsncArchiveConsumer \
                 acsncORBHelperImpl acsncHelperImpl \
                 acsncConsumerImpl acsncSimpleConsumerImpl \
                 acsncSupplierImpl acsncSimpleSupplierImpl acsncRTSupplierImpl \
                 acsncReconnectionCallback acsncCircularQueue
acsnc_LIBS     = pthread rt ACSErrTypeCommon acsncStubs acsncErrType AcsNCTraceLogLTS logging RepeatGuard acstime maci maciClient basenc TAO_CosNotification_MC_Ext

CDB_SCHEMAS = Channels EventChannel

XSDBIND = acsncSchemaBindings

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
