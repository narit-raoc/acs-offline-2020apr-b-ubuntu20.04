ACSERRDEF = acsncErrType

IDL_FILES = acsnc
IDL_TAO_FLAGS =
USER_IDL =

acsncStubs_LIBS = TAO_CosNotification_Skel acscommonStubs baciStubs

DEBUG=on

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
