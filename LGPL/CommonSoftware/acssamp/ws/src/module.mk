INCLUDES        = acssampImpl.h acssampObjImpl.h acssampObjImpl.i

LIBRARIES       = acssamp
LIBRARIES_L     =

acssamp_OBJECTS   = acssampImpl acssampC acssampS
acssamp_LIBS = acsutil cdb logging acscomponent baci maci maciClient acsnc acssampStubs TAO_DynamicInterface

CDB_SCHEMAS = SAMP

IDL_FILES = acssamp
IDL_TAO_FLAGS =
USER_IDL =

acssampStubs_LIBS = baciStubs ACSErrTypeCommon

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
