INCLUDES        = RepeatGuard.h RepeatGuardLogger.h

LIBRARIES       = RepeatGuard RepeatGuardLogger
LIBRARIES_L     =

RepeatGuard_OBJECTS   = RepeatGuard
RepeatGuard_LIBS      = acsutil acsThread

RepeatGuardLogger_OBJECTS   = RepeatGuardLogger
RepeatGuardLogger_LIBS   = RepeatGuard

JARFILES= repeatGuard
repeatGuard_DIRS=alma
repeatGuard_JARS:=acsjlog

DEBUG=on 

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
