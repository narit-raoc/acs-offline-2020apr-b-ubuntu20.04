SCRIPTS         = \
		acsStartContainer \
		acsStopContainer \
		acsStartORBSRVC acsStopORBSRVC \
		acsStartManager acsStopManager \
		acsStart acsStop \
		acsstartupAcsPorts acsstartupPids \
		acsStartJava \
		acsList \
		acsKillProc \
		irbrowser nsbrowser \
		acsStartLight \
		acsstartupConstants \
		acsstartupAcsInstance \
		acsstartupLogging.sh \
		acsstartupReport \
		acsStatus acsstartupLoadIFR \
		acsstartupProcesses \
		acsACSLogService \
		acsConfigurationDatabase \
		acsRDBConfigurationDatabase \
		acsInterfaceRepository \
		acsLoggingService \
		acsManager \
		acsNamingService \
		acsNotifyService acsstartupNotifyServiceStart \
		acsAlarmService \
		acsStartContainerWithFortran \
		acsstartupNotifyPortViaErrorCode \
		acsLogProcessMem \
		ifrCacheInvalidate irquery \
		acsstartupNSRef acsQueryNS \
		acsstartupFreeInstanceDir

SCRIPTS_L       =

PY_SCRIPTS         = killACS acsstartupContainerPort acsstartupNotifyPort \
		     acsstartupRemovePID \
		     acsContainersStatus acsNotifysStatus \
		     acsstartupCreateChannel \
                     acsdataClean
PY_SCRIPTS_L       =

EXECUTABLES = acsstartupIrFeed
MAKE_NOIFR_CHECK = on # jagonzal: there is a cyclic dependency between acsstartupIrFeed and acsstartupLoadIFR (checker)

acsstartupIrFeed_OBJECTS = acsstartupIrFeed

IDL_FILES = ACSIRSentinel

DEBUG= on

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
