DEBUG=on

EXECUTABLES     = acsLogSvc
EXECUTABLES_L   = 

acsLogSvc_OBJECTS   = acsLogSvc acslogSvcImpl
acsLogSvc_LIBS      = acserr maci logging acsutil acslogStubs

IDL_FILES = acslog
IDL_TAO_FLAGS =
USER_IDL =

acslogStubs_LIBS = acscommonStubs acserrStubs

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
