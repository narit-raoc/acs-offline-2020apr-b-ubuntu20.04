PY_SCRIPTS  = ACSPyConsole ACSStartContainerPy
PY_PACKAGES = Acspy acs

SCRIPTS   = acspyInteractiveContainer acsLoggingMonitor
SCRIPTS_L = 

CDB_SCHEMAS := EventConverter

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
