MAKE_NOIFR_CHECK = on # it is DDS IDL, and we do not need to check it

ifdef NDDSHOME
ARCH = $(shell uname -m)
GCC_VERSION_MAJOR = $(shell echo __GNUC__ | gcc -E -xc - | tail -n 1)
ifneq ($(GCC_VERSION_MAJOR),4)
        echo "ERROR: not supported gcc version for RTI DDS: $(GCC_VERSION_MAJOR)"
endif
GCC_VERSION_MINOR = $(shell echo __GNUC_MINOR__ | gcc -E -xc - | tail -n 1)
ifeq ($(GCC_VERSION_MINOR),1)
        RTIDDS_GCC_VER=4.1.2
else
        RTIDDS_GCC_VER=4.4.5
endif

USER_CFLAGS = -DRTI_UNIX -DRTI_LINUX -g -O0
USER_INC = -I$(NDDSHOME)/include -I$(NDDSHOME)/include/ndds
ifeq ($(ARCH),x86_64)
	USER_LIB = -L$(NDDSHOME)/lib/x64Linux2.6gcc$(RTIDDS_GCC_VER)
else
	USER_LIB = -L$(NDDSHOME)/lib/i86Linux2.6gcc$(RTIDDS_GCC_VER)
endif

EXECUTABLES     =  bulkDataNTGenSender bulkDataNTGenReceiver bulkDataNTGenReceiverSender ddsPubTest ddsSubTest
EXECUTABLES_L   = 

bulkDataNTGenSender_OBJECTS = bulkDataNTGenSender
bulkDataNTGenSender_LIBS= bulkDataNTSender acsnc bulkDataStubs

bulkDataNTGenReceiver_OBJECTS = bulkDataNTGenReceiver
bulkDataNTGenReceiver_LIBS= bulkDataNTReceiver 

bulkDataNTGenReceiverSender_OBJECTS = bulkDataNTGenReceiverSender bulkDataNTArrayThread bulkDataNTGenStreamerThread
bulkDataNTGenReceiverSender_LIBS= bulkDataNTSender bulkDataNTReceiver bulkDataNTThreadUtils bulkDataNTEx acsnc

ddsPubTest_OBJECTS=ddsPublisher 
ddsPubTest_LIBS=nddscpp nddsc nddscore pthread ACSErrTypeCommon

ddsSubTest_OBJECTS=ddsSubscriber
ddsSubTest_LIBS=nddscpp nddsc nddscore pthread ACSErrTypeCommon

INCLUDES        = bulkDataNTCallback.h bulkDataNTDDS.h bulkDataNTDDSPublisher.h bulkDataNTDDSSubscriber.h \
				bulkDataNT.h bulkDataNTPlugin.h bulkDataNTReaderListener.h bulkDataNTReceiverFlow.h \
				bulkDataNTReceiverImpl.h bulkDataNTReceiverImpl.i bulkDataNTReceiverStream.h bulkDataNTReceiverStream.i \
				bulkDataNTSenderFlow.h bulkDataNTSenderImpl.h bulkDataNTSenderStream.h bulkDataNTStream.h bulkDataNTSupport.h \
				bulkDataNTFlow.h bulkDataNTWriterListener.h bulkDataNTDDSLoggable.h bulkDataNTSenderFlowCallback.h \
				bulkDataNTConfiguration.h bulkDataNTConfigurationParser.h \
				bulkDataNTArrayThread.h bulkDataNTPosixHelper.h bulkDataNTThreadSyncGuard.h \
				bulkDataNTGenEx.h bulkDataNTProcessQueue.h

LIBRARIES       = bulkDataNT bulkDataNTSender bulkDataNTReceiver bulkDataNTSenderImpl bulkDataNTThreadUtils bulkDataNTEx	
LIBRARIES_L     =

bulkDataNT_OBJECTS   = bulkDataNTDDS bulkDataNTStream bulkDataNTConfiguration bulkDataNTConfigurationParser \
						bulkDataNT bulkDataNTSupport bulkDataNTPlugin bulkDataNTDDSLoggable bulkDataNTLibMgmt
bulkDataNT_LIBS = ACSErrTypeCommon ACS_BD_Errors ACS_DDS_Errors xerces-c  nddscpp nddsc nddscore acsnc

bulkDataNTSender_OBJECTS    = bulkDataNTSenderStream bulkDataNTSenderFlow bulkDataNTDDSPublisher \
							  bulkDataNTWriterListener bulkDataNTSenderFlowCallback
bulkDataNTSender_LIBS = bulkDataNT bulkDataSenderStubs baci acsnc

bulkDataNTSenderImpl_OBJECTS = bulkDataNTSenderImpl
bulkDataNTSenderImpl_LIBS = bulkDataNTSender acsnc

bulkDataNTThreadUtils_OBJECTS = bulkDataNTPosixHelper bulkDataNTThreadSyncGuard
bulkDataNTThreadUtils_LIBS = pthread ACE logging acsThread acstime

bulkDataNTEx_OBJECTS = bulkDataNTGenEx
bulkDataNTEx_LD_FLAGS = 
bulkDataNTEx_LIBS = acsnc 

bulkDataNTReceiver_OBJECTS   = bulkDataNTReceiverFlow bulkDataNTDDSSubscriber \
							   bulkDataNTReaderListener bulkDataNTCallback bulkDataNTProcessQueue
bulkDataNTReceiver_LIBS = bulkDataNT RepeatGuard bulkDataStubs

PY_SCRIPTS         = bulkDataNTremoteTest
PY_SCRIPTS_L       =

endif #ifdef NDDSHOME

CDB_SCHEMAS = BulkDataNTReceiver BulkDataNTSender acs_rti_dds_qos_profiles

IDL_FILES = bulkData bulkDataReceiver bulkDataSender bulkDataDistributer
TAO_IDLFLAGS =

bulkDataStubs_LIBS = TAO_AV

bulkDataReceiverStubs_LIBS = baciStubs ACSBulkDataError ACSBulkDataStatus bulkDataStubs

bulkDataSenderStubs_LIBS = baciStubs ACSBulkDataError bulkDataStubs bulkDataReceiverStubs

bulkDataDistributerStubs_LIBS = baciStubs ACSErrTypeCommon ACSBulkDataError bulkDataStubs bulkDataReceiverStubs bulkDataSenderStubs

ACSERRDEF = ACS_BD_Errors ACS_DDS_Errors ACSBulkDataError ACSBulkDataStatus

ifdef NDDSHOME
$(MODRULE)all: $(MODPATH) $(MODDEP) do_dds_gen
	$(AT)echo " . . . $@ done"
	
$(MODRULE)clean: $(MODPATH) clean_$(MODDEP) dds_clean
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)cp -f $(MODPATH)/config/bulkDataNTDefaultQosProfiles.*.xml $(INSTDIR)/config
	$(AT)chmod $(P644) $(INSTDIR)/config/bulkDataNTDefaultQosProfiles.*.xml
	$(AT)echo " . . . $@ done"
else
$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo -e "\e[0;31mWARNING: NDDSHOME is not defined, and thus only code common to old and new BD will be build (ACSERR and IDL) for target all!!!!\e[0m" 
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo -e "\e[0;31mWARNING: NDDSHOME is not defined, and thus only code common to old and new BD will be installed (ACSERR and IDL) for target install!!!!\e[0m"
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"
endif

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"

do_dds_gen: $(addprefix $(MODPATH)/src/,bulkDataNT.cpp bulkDataNTSupport.cpp bulkDataNTPlugin.cpp) $(addprefix $(MODPATH)/include/,bulkDataNT.h  bulkDataNTSupport.h bulkDataNTPlugin.h)
	
$(addprefix $(MODPATH)/src/,bulkDataNT.cpp bulkDataNTSupport.cpp bulkDataNTPlugin.cpp) $(addprefix $(MODPATH)/include/,bulkDataNT.h  bulkDataNTSupport.h bulkDataNTPlugin.h : ../idl/bulkDataNT.idl)
	$(NDDSHOME)/scripts/rtiddsgen -namespace -replace -d $(MODPATH)/src $(MODPATH)/idl/bulkDataNT.idl; \
	rename .cxx .cpp $(MODPATH)/src/*.cxx; \
	mv $(MODPATH)/src/*.h $(MODPATH)/include

dds_clean:
	$(AT)rm -f $(addprefix $(MODPATH)/src/,bulkDataNT.cpp bulkDataNTSupport.cpp bulkDataNTPlugin.cpp); \
	rm -f $(addprefix $(MODPATH)/include/,bulkDataNT.h bulkDataNTSupport.h bulkDataNTPlugin.h)
