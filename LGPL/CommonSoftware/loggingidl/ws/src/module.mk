IDL_FILES = logging_idl
IDL_TAO_FLAGS = 
USER_IDL = -I$(ACE_ROOT)/TAO
logging_idlStubs_LIBS = TAO_DsLogAdmin TAO_DsLogAdmin_Serv TAO_DsLogAdmin_Skel

DEBUG = on

$(MODRULE)all: $(MODPATH) $(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)install: $(MODPATH) install_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean: $(MODPATH) clean_$(MODDEP)
	$(AT)echo " . . . $@ done"

$(MODRULE)clean_dist: $(MODPATH) clean_dist_$(MODDEP)
	$(AT)echo " . . . $@ done"
